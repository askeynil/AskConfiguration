#!/bin/bash

# 1. 导入依赖
if [ ! -e define.sh ]; then
    # 当前目录没有依赖文件,下载依赖
    wget https://gitee.com/askeynil/AskConfiguration/raw/master/define.sh
    source define.sh
    rm -rf define.sh
else
    source define.sh
fi

# 2. 安装 zsh 的依赖
sudo apt install git


# 3. 安装 zsh 是否存在
if [ ! -e /usr/bin/zsh ]; then
    sudo apt install zsh -y
    success "zsh 安装成功"
else
    success "zsh 已经安装"
fi

# 4. 判断 oh-my-zsh
if [ ! -e ~/.oh-my-zsh ]; then
    # 不存在
    sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
    success "oh-my-zsh 安装成功"
else
    success "oh-my-zsh 已安装"
fi
