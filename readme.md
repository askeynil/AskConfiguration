# Ubuntu 下一些自动化安装脚本

> 所有安装均在 Ubuntu 16.04 下
> 以下脚本都默认已经安装过 curl 和更换了源，建议使用 Tsinghua 源



## 一键更换源脚本

```bash
sh -c "$(wget https://gitee.com/askeynil/AskConfiguration/raw/master/sources.sh -O -)"
```
该脚本可以加上指定源，参数 -n

同时支持 1604 和 1804的Ubuntu系统，会自动根据系统选择相应版本

当前支持的源如下：
1. aliyun (阿里云)
2. tsinghua (清华)
3. ustc (中科大)

默认使用 tsinghua 源

例如
``` bash
sh -c "$(wget https://gitee.com/askeynil/AskConfiguration/raw/master/sources.sh -O -)" -n aliyun
```




## sogou 安装脚本

```bash
sh -c "$(wget https://gitee.com/askeynil/AskConfiguration/raw/master/sogou.sh -O -)"
```



## zsh 安装脚本

```bash
sh -c "$(wget https://gitee.com/askeynil/AskConfiguration/raw/master/zsh.sh -O -)"
```



## 终端常用插件安装

1. autojump
2. zsh-autosuggestion
3. zsh-syntax-highlighting

```bash
sh -c "$(wget https://gitee.com/askeynil/AskConfiguration/raw/master/plugins.sh -O -)"
```



## ros 安装脚本

默认安装的是`kinetic`

```bash
sh -c "$(wget https://gitee.com/askeynil/AskConfiguration/raw/master/ros.sh -O -)"
```



## anaconda 安装脚本

该脚本默认会下载最新的 anaconda 版本进行安装
``` bash
sh -c "$(wget https://gitee.com/askeynil/AskConfiguration/raw/master/anaconda.sh -O -)"
```



## albert 安装脚本
``` bash
sh -c "$(wget https://gitee.com/askeynil/AskConfiguration/raw/master/albert.sh -O -)"
```

## typora 安装脚本
``` bash
sh -c "$(wget https://gitee.com/askeynil/AskConfiguration/raw/master/typora.sh -O -)"
```


## OpenCV 安装脚本

``` bash
sh -c "$(wget https://gitee.com/askeynil/AskConfiguration/raw/master/opencv.sh -O -)" -n 3.4.7
```

末尾可以指定版本号，版本号必须是在OpenCV仓库中存在的版本号，不然会出错