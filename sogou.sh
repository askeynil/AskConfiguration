#!/bin/bash

# 0. 导入依赖
if [ ! -e define.sh ]; then
    # 当前目录没有依赖文件,下载依赖
    wget https://gitee.com/askeynil/AskConfiguration/raw/master/define.sh
    source define.sh
    rm -rf define.sh
else
    source define.sh
fi

# 1. 判断搜狗拼音输入法是不是安装过了
if [ -n "`dpkg -l | grep sogoupinyin`" ]; then
    # body
    success "输入法已经安装了 不需要再次安装了"
    exit
fi

# 1.1 检查 curl 依赖
install curl

# 2. 下载 sogou 输入法安装包
# 2.1 获取 sogou 的下载连接
url=`curl https://pinyin.sogou.com/linux/\?r\=pinyin | grep -oE "http://cdn2.*amd64\.deb"`
filename=/tmp/sogou.deb
wget -O $filename $url

# 开始安装
# 3. 确认 fcitx 是否存在
if [ -z `which fcitx` ]; then
    # fcitx 不存在 安装 fcitx
    sudo apt install -y fcitx
fi

# 4. 安装搜狗输入法
count=0
sudo rm -f /var/lib/dpkg/lock
sudo dpkg -i $filename
while [ $? != 0 ]; do
    let count++
    if [ $count == 5 ]; then
        failure "搜狗安装失败，请手动尝试"
        exit
    fi
    loading "安装搜狗输入法出现依赖问题，安装所需依赖"
    # 安装出现依赖问题
    sudo rm -f /var/cache/apt/archives/lock
    sudo apt install -yf
done

# 5. 检查是否有 im-config 命令
if [ -z `which im-config` ]; then
    # im-config 不存在 安装 im-config
    sudo rm -f /var/cache/apt/archives/lock
    sudo apt-get install -y im-config fcitx-config-gtk
fi

# 6. 删除 sogou 安装包
rm -rf $filename

# 7. 设置fcitx
loading '在出现的界面中点“确定”->"yes"->选择fcitx->"确定"'
loading '安装完毕后，重启即可正常使用 搜狗输入法'
im-config

