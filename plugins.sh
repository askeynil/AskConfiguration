#!/bin/bash
### 
# Description: 一些常用插件
 # Author: AskeyNil
 # CreateDate: 2019-10-11 15:54:41
 # LastEditors: AskeyNil
 #
 # *********************************
 # **                             **
 # **     　  你只有足够努力     　  **
 # **       才能看上去毫不费力       **
 # **                             **
 # *********************************
 #
 ###

# 0. 导入依赖
if [ ! -e define.sh ]; then
    # 当前目录没有依赖文件,下载依赖
    wget https://gitee.com/askeynil/AskConfiguration/raw/master/define.sh
    source define.sh
    rm -rf define.sh
else
    source define.sh
fi

# ! 一共三个好用的插件
# 1. autojump https://github.com/wting/autojump 一个更快速的文件夹跳转命令
# 1.1 先判断该插件是否已安装
if [ -e ~/.autojump ]; then
    success "autojump 已安装"
else
    # 插件未安装 开始安装
    # 1.2 下载插件
    loading "开始下载 autojump"
    git clone https://github.com/wting/autojump.git

    # 1.3 安装命令
    loading "开始安装 autojump"
    cd autojump
    ./install.py
    cd ../
    rm -rf autojump
    cmd="[[ -s ~/.autojump/etc/profile.d/autojump.sh ]] && source ~/.autojump/etc/profile.d/autojump.sh"
    add_bash "$cmd"
    add_zsh "$cmd"
    success "autojump 安装成功"
fi



# zsh 的插件
# 判断是否安装 zsh
if [ ! -e /usr/bin/zsh ]; then
    failure "zsh 未安装, 不安装另外两个插件"
    exit
fi
# 2. zsh-autosuggestion https://github.com/zsh-users/zsh-autosuggestions
# 2.1 判断该插件是否已安装
if [ -e ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions ]; then
    success "zsh-autosuggestions 已安装"
else
    # 不存在
    git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
fi

# 3. zsh-syntax-highlighting
# 3.1 判断该插件是否已安装
if [ -e ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting ]; then
    success "zsh-syntax-highlighting 已安装"
else
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
fi

# 4. 将以上两个插件启动命令添加到zsh中
add_plugins(){
    if [ ! -n "`cat ~/.zshrc | grep "^plugins=\(.*$1.*\)"`" ]; then
        sed -i "s/plugins=(\(.*\))/plugins=(\1 $1)/g" ~/.zshrc
    fi
}

add_plugins zsh-autosuggestions
add_plugins zsh-syntax-highlighting
