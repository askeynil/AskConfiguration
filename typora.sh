#!/bin/bash


# 1. 导入依赖
if [ ! -e define.sh ]; then
    # 当前目录没有依赖文件,下载依赖
    wget https://gitee.com/askeynil/AskConfiguration/raw/master/define.sh
    source define.sh
    rm -rf define.sh
else
    source define.sh
fi

# 2. 判断是否有 software 文件夹
if [ ! -e ~/software ]; then
	# 没有 software 文件夹
	mkdir ~/software
fi

# 3. 判断是否安装，如果安装了就跳过  的安装
if [ -e ~/software/typora ]; then
	# 已经安装过了
	success "Typora 已经安装了，跳过安装"
	exit
fi

filename=/tmp/typora.tar.gz
# 4. 判断当前如今 deb 下是否有安装文件
if [ -e deb/Typora*.tar.gz ]; then
	# 拷贝到 tmp 目录下
	loading "从本地加载 typora 安装文件"
	cp deb/Typora*.tar.gz $filename
else
	loading "从网络下载 typora 安装文件"
	wget -O $filename https://typora.io/linux/Typora-linux-x64.tar.gz
fi

# 5. 解压到指定 software 文件夹
loading "开始解压 Typora 安装文件"
tar zxf $filename -C ~/software

# 6. 建立到 bin 下的软连接
if [ ! -e ~/bin ]; then
	# 没有 bin 文件夹
	mkdir ~/bin
fi
ln -s ~/software/Typora/Typora ~/bin/typora

success "Typora 安装完成"

# 7. 删除安装包
rm -rf $filename
-