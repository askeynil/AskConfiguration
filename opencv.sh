#!/usr/bash


# 0. 导入依赖
if [ ! -e define.sh ]; then
    # 当前目录没有依赖文件,下载依赖
    wget https://gitee.com/askeynil/AskConfiguration/raw/master/define.sh
    source define.sh
    rm -rf define.sh
else
    source define.sh
fi


# # 1. OpenCV 必备依赖
sudo apt install build-essential -y
sudo apt install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev -y
sudo apt install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev -y

# 2. 获取当前版本

version=`echo $*`
[ -z $version ] && version=3.4.7

loading "当前的设置的版本为 $version"

filename=/tmp/OpenCVBuild
if [ ! -e $filename ]; then
    mkdir $filename
fi

# 3. 下载指定版本的 OpenCV
git clone --branch $version https://github.com/opencv/opencv_contrib.git $filename/opencv_contrib-$version
git clone --branch $version https://github.com/opencv/opencv.git $filename/opencv-$version

# 4. 验证 OpenCV 是否下载成功
if [ ! -e $filename/opencv-$version ]; then
    # 下载失败，请验证版本信息后重试
    failure "下载失败，请验证版本信息后重试（当前输入版本：$version）"
    exit
fi

# 5. 进入到需要安装的目录
rm -rf $filename/build
mkdir $filename/build
cd $filename/build

# 6. 开始安装
cmake -D CMAKE_BUILD_TYPE=RELEASE \
      -D CMAKE_INSTALL_PREFIX=~/devtools/libs/opencv-$version \
      -D OPENCV_EXTRA_MODULES_PATH=$filename/opencv_contrib-$version/modules \
      -D BUILD_DOCS=ON ../opencv-$version

# 7. make
make -j8
make install

# 8. 回到原文件夹，并删除下载的文件。
cd -
rm -rf $filename/build
mv $filename/ ~/devtools/libs/opencv-$version/
success "OpenCV 安装成功"




