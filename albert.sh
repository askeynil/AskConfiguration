#!/bin/bash

# 0. 导入依赖
if [ ! -e define.sh ]; then
    # 当前目录没有依赖文件,下载依赖
    wget https://gitee.com/askeynil/AskConfiguration/raw/master/define.sh
    source define.sh
    rm -rf define.sh
else
    source define.sh
fi

# 1. 判断是否已经安装
is_install=`which albert`
if [ $is_install ]; then
    success "albert 已安装"
    exit
fi

# 2. 使用官方提供的方式安装
# https://software.opensuse.org/download.html?project=home:manuelschneid3r&package=albert
# 官网提供的安装方式  https://albertlauncher.github.io/docs/installing/
sudo rm -f /var/cache/apt/archives/lock
sudo sh -c "echo 'deb http://download.opensuse.org/repositories/home:/manuelschneid3r/xUbuntu_16.04/ /' > /etc/apt/sources.list.d/home:manuelschneid3r.list"
wget -nv https://download.opensuse.org/repositories/home:manuelschneid3r/xUbuntu_16.04/Release.key -O Release.key
sudo apt-key add - < Release.key
rm -rf Release.key
sudo apt update
sudo apt install -y albert
success "albert 安装成功"